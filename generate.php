<?php
$doc = file_get_contents('words.csv');
$lines = explode("\r", $doc);
$gen = new IMGenerator($lines);

class IMGenerator {
	private $image;
	private $charHeight = 25;
	private $font = './Capsmall_clean.ttf';
	private $bad  = array();
	private $good = array();

	public function __construct($words){
		$n = count($words);
		for($i = 0; $i < $n ; $i++){
			if(substr($words[$i], 0, 1) != '#'){
				$spl = explode(';', $words[$i]);
				if($spl[1] == 1){
					$this->good[] = $spl[0];
				}else{
					$this->bad[] = $spl[0];
				}
			}
		}
	}

	public function render(){
		return json_encode(array(
			'good' => $this->good,
			'bad'  => $this->bad,
		));
	}
}
